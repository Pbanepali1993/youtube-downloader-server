import youtube from "youtube-mp3-downloader"
import express from "express"
import fs from "fs"
import env from './config'

const server = express()

server.get('/:link', (req, res) => {

    let linkId = req.params.link

    try {
        if (fs.existsSync("./download/" + linkId + ".mp3")) {
            res.download("./download/" + linkId + ".mp3", linkId + ".mp3")
        } else {
            //Configure YoutubeMp3Downloader with your settings
            var YD = new youtube({
                "ffmpegPath": "ffmpeg/bin/ffmpeg.exe",        // Where is the FFmpeg binary located?
                "outputPath": "download",    // Where should the downloaded and encoded files be stored?
                "youtubeVideoQuality": "highest",       // What video quality should be used?
                "queueParallelism": 2,                  // How many parallel downloads/encodes should be started?
                "progressTimeout": 2000                 // How long should be the interval of the progress reports
            })

            //Download video and save as MP3 file
            YD.download(linkId, linkId + ".mp3");

            YD.on("finished", function (err, data) {
                console.log(JSON.stringify(data));
                res.download("./download/" + linkId + ".mp3", data.title + ".mp3")
            })

            YD.on("error", function (error) {
                console.log(error);
                res.send("Error")
            })

            YD.on("progress", function (progress) {
                console.log(JSON.stringify(progress));
            })
        }
    } catch (err) {
        console.error(err)
    }
})

server.listen(env.port, () =>
    console.log('Example app listening on port ' + env.port + '!'),
);