# Youtube Downloader Server

This is an experimental server application to download mp3 music from youtube videos. For learning purposes only.

## Configuration

You need to download and extract [ffmpeg](https://ffmpeg.org/) to the lib folder. The name of the folder needs to be ``ffmpeg`` and must contain a ``bin`` folder, with ffmpeg executable.

## Run

To run this at development, just run ``npm install`` and ``npm start``